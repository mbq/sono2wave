use byteorder::{LittleEndian, ReadBytesExt};
use hound;
use std::convert::From;
use std::io::Seek;
use std::path::PathBuf;
use std::{fs, io};
use structopt::StructOpt;

#[derive(Debug)]
enum Err {
    Hound(hound::Error),
    Io(io::Error),
    TooShort,
}

impl From<hound::Error> for Err {
    fn from(x: hound::Error) -> Self {
        Err::Hound(x)
    }
}
impl From<io::Error> for Err {
    fn from(x: io::Error) -> Self {
        Err::Io(x)
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opt {
    ///Output file name
    #[structopt(short = "o", long = "output", parse(from_os_str))]
    output: Option<PathBuf>,

    ///File to process
    #[structopt(name = "FILE", parse(from_os_str))]
    input: PathBuf,
}
impl Opt {
    fn actual_output(&self) -> PathBuf {
        if self.output.is_none() {
            let mut input = self.input.clone();
            input.set_extension("wav");
            input
        } else {
            self.output.clone().unwrap()
        }
    }
    fn convert(&self) -> Result<(), Err> {
        let sample_rate = 250_000_u64;
        let interlude_sec_bytes = 24_u64;
        let header_bytes = 212_u64;
        let spec = hound::WavSpec {
            channels: 1,
            sample_rate: sample_rate as u32,
            bits_per_sample: 16,
            sample_format: hound::SampleFormat::Int,
        };
        let flen = fs::metadata(&self.input)?.len();

        if flen > header_bytes {
            let mut source = std::io::BufReader::new(fs::File::open(&self.input)?);
            //Ignore sonotrack header, 234 bytes
            source.seek(std::io::SeekFrom::Start(header_bytes))?;
            let mut sink = hound::WavWriter::create(self.actual_output(), spec)?;

            let seconds = (flen - header_bytes) / (sample_rate * 2 + interlude_sec_bytes);
            for _ in 0..seconds {
                source.seek(std::io::SeekFrom::Current(interlude_sec_bytes as i64))?;
                for _ in 0..sample_rate {
                    let samp = source.read_i16::<LittleEndian>()?;
                    sink.write_sample(samp)?;
                }
            }
            Ok(())
        } else {
            Err(Err::TooShort)
        }
    }
}

fn main() {
    let opt = Opt::from_args();
    eprintln!(
        "Converting {:?} into {:?}...",
        opt.input,
        opt.actual_output()
    );
    println!("{:?}", opt.convert());
}
